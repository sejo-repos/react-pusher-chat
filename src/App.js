import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import Pusher from "pusher-js";
import axios from "axios";

import ChatList from "./components/ChatList";
import ChatBox from "./components/ChatBox";

function App() {
  const [text, setText] = useState("");
  const [chats, setChats] = useState([]);

  useEffect(() => {
    const pusher = new Pusher("f74d237275b419c3f926", {
      cluster: "us2",
      encrypted: true,
    });

    const channel = pusher.subscribe("chat");
    channel.bind("message", (data) => {
      setChats([...chats, data]);
    });
  }, [chats]);

  async function handleClick() {
    const payload = {
      message: text,
    };
    await axios.post("http://localhost:5000/message", payload);
    setText('')
  }

  return (
    <>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Chat <code>Reack.js</code> and pusher.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
      <div className="container">
        <ChatList chats={chats} />
        <ChatBox setText={setText} text={text} handleClick={handleClick}/>
      </div>
    </>
  );
}

export default App;
