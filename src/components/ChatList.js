import React from "react";

function ChatList({ chats }) {
  return (
    <div className="container mt-1">
      {chats.map((chat,index) => {
        return (
          <div key={index} className="mb-1">
            <div className="row">
              <div className="col-xs-12 border rounded w-100">
                  <div key={chat.id} className="d-flex align-items-center p-3">
                      <div className="text-monospace">{chat.message}</div>
                  </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default ChatList;
