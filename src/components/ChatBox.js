import React from "react";

function ChatBox({ setText, text, handleClick }) {

  
  return (
    <div>
      <div className="row">
        <div className="col-xs-12 mx-auto">
          <h3 className="greetings mt-2">Hello :)</h3>
          <div className="chat">
            <div className="col-xs-5 col-xs-offset-3 mt-3">
              <input
                type="text"
                value={text}
                placeholder="chat here..."
                className="form-control"
                onChange={(e)=>setText(e.target.value)}
              />
              <button className="btn btn-primary btn-block mt-3" onClick={handleClick}>Send</button>
            </div>
            <div className="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ChatBox;
